import asyncio
import logging
from kiyona import DEFAULT_SERVICE_CONFIG
from kiyona.transport.kafka import KafkaRpcClient

LOGGER = logging.getLogger(__name__)


class RpcClientProxy:
    def __init__(
        self,
        service_name,
        service_config=DEFAULT_SERVICE_CONFIG,
        loop=asyncio.get_event_loop()
    ):
        self._client = KafkaRpcClient(service_name, service_config, loop)

    async def __aenter__(self):
        await self._client.start()
        return self

    async def __aexit__(self, exc_type, exc, tb):
        await self._client.stop()

    def __getattr__(self, attr_name):
        return getattr(self._client, attr_name)
