import logging
import sys
import signal
from kiyona.transport.kafka import KafkaTransport
# from kiyona.transport.http import HttpTransport

LOGGER = logging.getLogger(__name__)


DEFAULT_SERVICE_CONFIG = {
    "KAFKA_SERVERS": ["localhost:9092"]
}


class Kiyona:
    supported_transports = {
        "kafka": KafkaTransport,
        # "http": HttpTransport,
    }

    def __init__(self, service_name, loop, service_config_file=None):
        self._loop = loop
        self._service_name = service_name
        self._transports = []

        # TODO: read file if it's set
        self._service_config = DEFAULT_SERVICE_CONFIG

        self._set_up_transports()

    def _set_up_transports(self):
        for transport_name, transport_cls in self.supported_transports.items():
            self._add_transport(transport_name, transport_cls)

    def _add_transport(self, transport_name, transport_cls):
        transport = transport_cls(
            self._service_name, self._service_config, self._loop
        )
        setattr(self, transport_name, transport)
        self._transports.append(transport)

    async def start(self):
        for transport in self._transports:
            await transport.start()

    async def stop(self):
        for transport in self._transports:
            await transport.stop()

    def run(self):
        LOGGER.setLevel(logging.DEBUG)

        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        )
        handler.setFormatter(formatter)
        LOGGER.addHandler(handler)

        class GracefulExitException(SystemExit):
            pass

        def termination_handler():
            raise GracefulExitException

        self._loop.create_task(self.start())
        self._loop.add_signal_handler(signal.SIGTERM, termination_handler)

        try:
            self._loop.run_forever()
        except KeyboardInterrupt:
            LOGGER.info("Received KeyboardInterrupt")
        except GracefulExitException:
            LOGGER.info("Received termination signal")
        finally:
            stop_task = self._loop.create_task(self.stop())
            self._loop.run_until_complete(stop_task)
