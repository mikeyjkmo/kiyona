import asyncio
import collections
import functools
import logging
import traceback
import uuid

import msgpack
from kafka.common import KafkaError
from aiokafka import AIOKafkaConsumer, AIOKafkaProducer
from aiokafka.errors import ConsumerStoppedError

LOGGER = logging.getLogger(__name__)

RpcResult = collections.namedtuple(
    "RpcResult", ("call_id", "partition_id", "value")
)


class RemoteError(Exception):
    pass


def decode_key(x):
    return x.decode("utf-8") if x else None


def encode_key(x):
    return x.encode("utf-8") if x else None


def decode_message(binary_msg):
    return msgpack.unpackb(binary_msg, raw=False)


def encode_message(message):
    """
    TODO: Support datetime serialization
    """
    return msgpack.packb(message)


class KafkaRpcClient:
    def __init__(self, service_name, service_config, loop):
        self._loop = loop
        self._service_name = service_name
        self._service_config = service_config

        # RPC related members
        self._request_topic = f"{self._service_name}_rpc"
        self._response_topic = f"{self._service_name}_rpc_response"

        self._partition_id = None
        self._request_producer = None
        self._response_consumer = None

        self._pending_calls = {}
        self._stopping_future = asyncio.Future()

    async def start(self):
        kafka_servers = self._service_config["KAFKA_SERVERS"]
        response_topic = self._response_topic

        self._response_consumer = AIOKafkaConsumer(
            response_topic,
            loop=self._loop,
            bootstrap_servers=kafka_servers,
            key_deserializer=decode_key,
            value_deserializer=decode_message,
        )
        self._request_producer = AIOKafkaProducer(
            bootstrap_servers=kafka_servers,
            loop=self._loop,
            key_serializer=encode_key,
            value_serializer=encode_message,
        )

        LOGGER.info("Starting RPC request producer...")
        await self._request_producer.start()

        LOGGER.info("Starting RPC response consumer...")
        await self._response_consumer.start()

        first_tp = None
        for tp in self._response_consumer.assignment():
            first_tp = tp
            break

        self._partition_id = first_tp.partition

        self._loop.create_task(self._consume_responses())

    async def stop(self):
        self._stopping_future.set_result(True)

        LOGGER.info("Stopping RPC request producer...")
        await self._request_producer.stop()

        LOGGER.info("Stopping RPC response consumer...")
        await self._response_consumer.stop()

    def _consumer_single_response(self, message):
        LOGGER.debug("Received response message: %s", message)

        call_id = message.key
        if call_id in self._pending_calls:
            value = message.value

            if value.get("error_type", None):
                raise RemoteError("RemoteError occurred")
            self._pending_calls[call_id].set_result(
                value["result"]
            )
            self._pending_calls.pop(call_id)

    async def _consume_set_of_responses(self):
        try:
            data = await self._response_consumer.getmany(timeout_ms=10)
            for messages in data.values():
                for message in messages:
                    self._consumer_single_response(message)
        except (asyncio.CancelledError, ConsumerStoppedError):
            if not self._stopping_future.done():
                LOGGER.exception("Consumption cancelled unexpectedly")
        except Exception:
            LOGGER.exception("Unexpected error whilst consuming responses")
        else:
            return False
        return True

    async def _consume_response_loop(self):
        while True:
            await self._consume_set_of_responses()

    async def _consume_responses(self):
        consume_task = self._loop.create_task(self._consume_response_loop())
        await self._stopping_future
        consume_task.cancel()

    def __getattr__(self, attr_name):
        async def _make_rpc_call(*args, **kwargs):
            call_id = str(uuid.uuid4())
            call = {
                "fname": attr_name,
                "partid": self._partition_id,
                "args": args,
                "kwargs": kwargs
            }
            LOGGER.debug("Sending request payload: %s", call)
            await self._request_producer.send(
                self._request_topic,
                call,
                key=call_id,
            )

            future = asyncio.Future()
            self._pending_calls[call_id] = future
            result = await future
            return result

        return _make_rpc_call


class KafkaRpc:
    def __init__(self, service_name, service_config, loop, use_group):
        self._loop = loop
        self._service_name = service_name
        self._service_config = service_config

        # RPC related members
        self._request_topic = f"{self._service_name}_rpc"
        self._request_group = (
            f"{self._request_topic}_group" if use_group else None
        )
        self._response_topic = f"{self._service_name}_rpc_response"

        self._result_queue = asyncio.Queue(loop=loop)

        self._initialised = False
        self._request_consumer = None
        self._response_producer = None

        self._handlers = {}
        self._pending_tasks = {}

        self._stopping_future = asyncio.Future()
        self._request_consumer_stopped = asyncio.Future()
        self._response_producer_stopped = asyncio.Future()

    def _initialise(self):
        if self._initialised:
            return

        kafka_servers = self._service_config["KAFKA_SERVERS"]
        request_topic = self._request_topic
        request_group = self._request_group

        self._request_consumer = AIOKafkaConsumer(
            request_topic,
            loop=self._loop,
            bootstrap_servers=kafka_servers,
            group_id=request_group,
            key_deserializer=decode_key,
            value_deserializer=decode_message,
        )
        self._response_producer = AIOKafkaProducer(
            bootstrap_servers=kafka_servers,
            loop=self._loop,
            key_serializer=encode_key,
            value_serializer=encode_message,
        )

        self._initialised = True

    def __call__(self, func):
        self._initialise()
        self._handlers[func.__name__] = func
        return func

    def _queue_result(self, call_id, partition_id, payload):
        self._pending_tasks.pop(call_id, None)

        LOGGER.debug("Queuing result: %s", payload)
        self._loop.create_task(
            self._result_queue.put(
                RpcResult(call_id, partition_id, payload)
            )
        )

    def _queue_error_response(self, call_id, partition_id, exception):
        tb = exception.__traceback__
        tb_list = traceback.format_tb(tb)

        for error in tb_list:
            LOGGER.error(error)

        payload = {
            "error_type": exception.__class__.__name__,
            "error_value": str(exception),
            "stack": tb_list,
        }

        return self._queue_result(
            call_id, partition_id, payload
        )

    def _queue_response(self, call_id, partition_id, task):
        exception = task.exception()
        if exception:
            return self._queue_error_response(
                call_id, partition_id, exception
            )

        payload = {
            "result": task.result()
        }

        LOGGER.debug("_queue_response payload %s", payload)
        return self._queue_result(
            call_id, partition_id, payload
        )

    def _process_single_incoming_request(self, message):
        try:
            call_id = message.key
            call = message.value

            LOGGER.debug(
                "Received request: call_id: %s, payload: %s",
                call_id, call
            )
            func_name = call["fname"]
            partition_id = call["partid"]
            args = call["args"]
            kwargs = call["kwargs"]

            func = self._handlers.get(func_name, None)

            if func is None:
                raise NameError(f"name '{func_name}' is not defined")

            if not asyncio.iscoroutinefunction(func):
                raise RuntimeError(
                    f"'{func_name}' should be a coroutine"
                )

            task = self._loop.create_task(func(*args, **kwargs))
            task.add_done_callback(
                functools.partial(
                    self._queue_response, call_id, partition_id
                )
            )

            self._pending_tasks[call_id] = task
        except Exception as exception:
            LOGGER.exception("Error when processing single request")
            self._queue_error_response(call_id, partition_id, exception)

    async def _process_incoming_request(self):
        try:
            data = await self._request_consumer.getmany(timeout_ms=10)
            for messages in data.values():
                for message in messages:
                    self._process_single_incoming_request(message)

        except (ConsumerStoppedError, asyncio.CancelledError):
            if not self._stopping_future.done():
                LOGGER.exception("Consumer stopped unexpectedly")
        except Exception:
            LOGGER.exception("Error when processing request")
        else:
            return False
        return True

    async def _process_incoming_requests(self):
        stopping = False
        while not stopping:
            stopping = await self._process_incoming_request()

    async def _consume_request_messages(self):
        def consume_exception(task):
            exception = task.exception()
            if not isinstance(exception, KeyboardInterrupt):
                LOGGER.exception(exception)

        process_task = self._loop.create_task(
            self._process_incoming_requests()
        )
        process_task.add_done_callback(consume_exception)
        await self._stopping_future
        process_task.cancel()

    async def _consume_requests(self):
        LOGGER.info("Consuming RPC requests...")

        await self._consume_request_messages()

        LOGGER.info("Stopping RPC request consumer...")
        await self._request_consumer.stop()
        LOGGER.info("RPC request consumer stopped.")
        self._request_consumer_stopped.set_result(True)

    async def _send_next_result(self):
        result = await self._result_queue.get()
        try:
            await self._response_producer.send(
                self._response_topic,
                result.value,
                key=result.call_id,
                partition=result.partition_id
            )
        except KafkaError:
            LOGGER.exception("Failed to send RPC response.")
        except asyncio.CancelledError:
            if not self._stopping_future.done():
                LOGGER.exception("Producer stopped unexpectedly")
        except Exception:
            LOGGER.exception("Exception occurred whilst sending response")
        else:
            return False
        return True

    async def _send_results(self):
        stopping = False
        while not stopping:
            stopping = await self._send_next_result()

    async def _produce_response_messages(self):
        send_task = self._loop.create_task(self._send_results())
        await self._stopping_future
        send_task.cancel()

    async def _produce_responses(self):
        LOGGER.info("Producing RPC responses...")

        await self._produce_response_messages()

        LOGGER.info("Stopping RPC response producer...")
        await self._response_producer.stop()
        LOGGER.info("RPC response producer stopped.")
        self._response_producer_stopped.set_result(True)

    async def _start_rpc(self):
        LOGGER.info("Starting RPC response producer...")
        await self._response_producer.start()

        LOGGER.info("Starting RPC request consumer...")
        await self._request_consumer.start()

        self._loop.create_task(self._consume_requests())
        self._loop.create_task(self._produce_responses())

    async def _stop_rpc(self):
        if not self._stopping_future.done():
            self._stopping_future.set_result(True)

        await asyncio.wait(
            [self._request_consumer_stopped, self._response_producer_stopped],
            return_when=asyncio.ALL_COMPLETED
        )

    async def start(self):
        if self._initialised:
            await self._start_rpc()

    async def stop(self):
        if self._initialised:
            await self._stop_rpc()


class KafkaTransport:
    def __init__(self, service_name, service_config, loop, use_group=True):
        self.rpc = KafkaRpc(service_name, service_config, loop, use_group)

    async def start(self):
        await self.rpc.start()

    async def stop(self):
        await self.rpc.stop()
