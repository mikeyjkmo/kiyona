import uuid
import pytest

from aiokafka import AIOKafkaConsumer, AIOKafkaProducer
from kiyona.transport.kafka import (
    KafkaTransport, decode_message, encode_message
)

# All test coroutines will be treated as marked.
pytestmark = pytest.mark.asyncio


@pytest.fixture
def service_name():
    return "test_service"


@pytest.fixture
def service_config():
    return {
        "KAFKA_SERVERS": ["localhost:9092"]
    }


@pytest.fixture
async def kafka_transport(service_name, service_config, event_loop):

    transport = KafkaTransport(
        service_name, service_config, loop=event_loop, use_group=False
    )

    @transport.rpc
    async def hello(name):
        return f"Hello {name}"

    @transport.rpc
    async def throw_name_error():
        raise NameError("This is a test exception")

    @transport.rpc
    def not_coroutine():
        return "This is not a coroutine"

    await transport.start()
    yield transport
    await transport.stop()


@pytest.fixture
def kafka_servers(service_config):
    return service_config["KAFKA_SERVERS"]


@pytest.fixture
async def consumer(kafka_transport, kafka_servers, event_loop):
    topic = kafka_transport.rpc._response_topic
    consumer = AIOKafkaConsumer(
         topic,
         loop=event_loop,
         bootstrap_servers=kafka_servers,
         # group_id="test_consumer_group",
         key_deserializer=lambda x: x.decode("utf-8"),
         value_deserializer=decode_message,
    )
    await consumer.start()
    yield consumer
    await consumer.stop()


@pytest.fixture
async def producer(kafka_servers, event_loop):
    producer = AIOKafkaProducer(
        bootstrap_servers=kafka_servers,
        loop=event_loop,
        key_serializer=lambda x: x.encode("utf-8"),
        value_serializer=encode_message,
    )
    await producer.start()
    yield producer
    await producer.stop()


@pytest.fixture
def make_rpc_call(kafka_transport, producer):
    async def _make_rpc_call(fname, *args, **kwargs):
        call_id = str(uuid.uuid4())
        call = {
            "fname": fname,
            "partid": 0,
            "args": args,
            "kwargs": kwargs
        }
        await producer.send(
            kafka_transport.rpc._request_topic,
            call,
            key=call_id,
        )
        return call_id
    return _make_rpc_call


@pytest.mark.parametrize(
    "rpc_input",
    ["World", "Bob", "Alice"]
)
async def test_hello(
    rpc_input, make_rpc_call, consumer
):
    call_id = await make_rpc_call("hello", rpc_input)

    message = await consumer.getone()

    assert message.key == call_id
    assert message.value["result"] == f"Hello {rpc_input}"


async def test_throw_name_error(make_rpc_call, consumer):
    call_id = await make_rpc_call("throw_name_error")

    message = await consumer.getone()

    assert message.key == call_id
    assert message.value["error_type"] == "NameError"


async def test_bad_arguments(make_rpc_call, consumer):
    call_id = await make_rpc_call("hello")

    message = await consumer.getone()

    assert message.key == call_id
    assert message.value["error_type"] == "TypeError"
    assert message.value["error_value"] == (
        "hello() missing 1 required positional argument: 'name'"
    )


async def test_not_coroutine(make_rpc_call, consumer):
    call_id = await make_rpc_call("not_coroutine")

    message = await consumer.getone()

    assert message.key == call_id
    assert message.value["error_type"] == "RuntimeError"
    assert message.value["error_value"] == (
        "'not_coroutine' should be a coroutine"
    )


async def test_func_does_not_exist(make_rpc_call, consumer):
    call_id = await make_rpc_call("non_exsiting_func")

    message = await consumer.getone()

    assert message.key == call_id
    assert message.value["error_type"] == "NameError"
    assert message.value["error_value"] == (
        "name 'non_exsiting_func' is not defined"
    )
