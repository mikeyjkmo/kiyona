import asyncio
from kiyona.standalone.kafka import RpcClientProxy

loop = asyncio.get_event_loop()


async def send_hello():
    async with RpcClientProxy("hello_service", loop=loop) as rpc:
        print(await rpc.hello("Bob"))


loop.run_until_complete(send_hello())
loop.close()
