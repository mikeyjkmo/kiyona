import asyncio
from kiyona import Kiyona

loop = asyncio.get_event_loop()
app = Kiyona("hello_service", loop=loop)


@app.kafka.rpc
async def hello(name=None):
    if name:
        return f"Hello {name}!"
    return "Hello World!"


app.run()
