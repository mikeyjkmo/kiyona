# -*- coding: utf-8 -*-
"""
Setup Kiyona
"""
from setuptools import setup, find_packages

setup(
    name="kiyona",
    author="Mikey Mo",
    author_email="fadeout7@gmail.com",
    url="https://gitlab.com/mikeyjkmo/kiyona",
    version="0.1",
    packages=find_packages(),
    install_requires=(
        "aiokafka",
        "msgpack",
        "uvloop",
    ),
    extras_require={"dev": (
        "pytest", "flake8", "pytest-cov", "pytest-asyncio"
    )},
    description=(
        "An asyncio microservice framework."
    ),
)
